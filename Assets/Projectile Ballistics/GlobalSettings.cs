﻿/*
 * Global settings. Stores all important settings in one place
*/
namespace ProjectileBallistics
{
    public static class GlobalSettings
    {
        public static class Update
        {
            public const float Rate = 200.0f; //Updates Per Second for every projectile. Lower values will improve performance
            public const float MaxAllowedTime = 1 / 3f; //If user CPU is slow, device will try to not exceed this time limit by stopping updating projectiles. User can see that bullets move more slowly [Seconds]
        }

        public static class HitDetection
        {
            //Exit hit detection
            public const float BackRayStep = 0.5f; //Precision of cavity detection algorithm [Meters]
            public const float BackRayMaxDistance = 2f; //Distance at when algorithm will give up trying to find cavities and will use simpler method which ignores their possible presence [Meters]
        }

        public static class Effects
        {
            public const float DecalOffset = 0.0025f; //Decal offset [Meters]
            public const float ParticleSystemOffset = 0.01f; //Particle system offset [Meters]
        }

        public static class Debug
        {
            public const bool LogMessages = false; //Give info about collisions or other events? May affect performance
            public const bool WarningMessages = true; //Give warning messages? May affect performance
        }

        public static class Pooling
        {
            public const int DefaultObjectCapacity = 200; //How many objects per prefab can be stored in the object pool
            public const bool ForceDecalPooling = false;
            public const bool ForceVFXPooling = false;
            public const bool ForceSFXPooling = false;
        }

        public static class Examples
        {
            public const bool ForcePooling = false;
        }
    }
}
