﻿using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace ProjectileBallistics.Pooling
{
    public delegate void DespawnEvent(PrefabEntry entry, PoolObject caller);

    [System.Serializable]
    public class PrefabEntry
    {
        public GameObject Prefab;
        public int Capacity = GlobalSettings.Pooling.DefaultObjectCapacity;

        protected Stack<GameObject> AvailableObjects = new Stack<GameObject>();

        public PrefabEntry()
        {
            Prefab = null;
            Capacity = GlobalSettings.Pooling.DefaultObjectCapacity;

            AvailableObjects = new Stack<GameObject>(Capacity);
        }
        public PrefabEntry(GameObject prefab, int capacity)
        {
            Prefab = prefab;
            Capacity = capacity;

            AvailableObjects = new Stack<GameObject>(Capacity);
        }

        public bool TryAddAvailableObject(GameObject instance)
        {
            if (AvailableObjects.Count < Capacity)
            {
                AvailableObjects.Push(instance);
                return true;
            }

            return false;
        }

        public bool RetrieveAvailable()
        {
            return AvailableObjects.Count > 0;
        }

        public GameObject TryRetrieveObject()
        {
            GameObject Value = null;

            while (Value == null && AvailableObjects.Count != 0)
            {
                Value = AvailableObjects.Pop();

                if (Value == null)
                {
                    ProjectileDebug.LogWarning(string.Format("Pooling System has encountered null value. One of [{0}] prefab instances might be destroyed instead of being disabled.", ProjectileDebug.Object(Prefab)), null);
                }
            }

            return Value;
        }
    }

    [AddComponentMenu("Projectile Ballistics/Pooling System")]
    public class PoolingSystem : MonoBehaviour
    {
        public int ObjectCapacity = GlobalSettings.Pooling.DefaultObjectCapacity;
        public List<PrefabEntry> Entries = new List<PrefabEntry>();

        private static PoolingSystem instance;
        public static PoolingSystem Instance
        {
            get
            {
                //Reference equals is very performant, but won't work with destroyed objects properly: OnDestroy() contains solution
                if (ReferenceEquals(instance, null))
                {
                    instance = FindObjectOfType<PoolingSystem>();
                    if (ReferenceEquals(instance, null))
                    {
                        instance = new GameObject("Pooling System").AddComponent<PoolingSystem>();
                    }
                }
                return instance;
            }
        }
        void OnDestroy()
        {
            instance = null;
        }

        public GameObject Create(GameObject original, Vector3 position, Quaternion rotation)
        {
            //Pooling
            PrefabEntry entry = TryFindEntry(original);
            if (entry == null)
            {
                entry = new PrefabEntry(original, ObjectCapacity);
                Entries.Add(entry);
            }

            GameObject objectInstance = entry.TryRetrieveObject();
            if (objectInstance != null)
            {
                objectInstance.transform.position = position;
                objectInstance.transform.rotation = rotation;

                objectInstance.gameObject.SetActive(true);
            }
            else
            {
                objectInstance = Instantiate(original, position, rotation);

                PoolObject poolObject = objectInstance.AddComponent<PoolObject>();
                poolObject.Initialize(entry);
                poolObject.OnDespawn += OnDespawn;
            }

            return objectInstance;
        }
        public T Create<T>(T original, Vector3 position, Quaternion rotation) where T : Component
        {
            GameObject originalObj = original.gameObject;

            GameObject objectInstance = Create(originalObj, position, rotation);
            T Value = objectInstance.GetComponent<T>();

            if (Value == null)
            {
                Debug.LogError("Pool Instance of the object doesn't contain requested component.", this);
            }

            return Value;
        }

        protected void OnDespawn(PrefabEntry entry, PoolObject caller)
        {
            if (!entry.TryAddAvailableObject(caller.gameObject))
            {
                Destroy(caller.gameObject);
            }
        }

        public PrefabEntry TryFindEntry(GameObject prefab)
        {
            int count = Entries.Count;
            for(int i = 0; i < count; i++)
            {
                if(Entries[i].Prefab == prefab)
                {
                    return Entries[i];
                }
            }

            return null;
        }

#if UNITY_EDITOR
        [MenuItem("GameObject/Projectile Ballistics/Pooling System", false, 49)]
        static void CreateUpdateManager()
        {
            GameObject obj = new GameObject("Pooling System");
            obj.AddComponent<PoolingSystem>();
            Selection.activeGameObject = obj;
        }
#endif
    }
}