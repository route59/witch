﻿using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

/*
 * Updates all projectiles by using one Update call. This is good for performance.
*/
namespace ProjectileBallistics
{
    using pDebug = ProjectileDebug;
    [AddComponentMenu("Projectile Ballistics/Projectile Update Manager")]
    public class ProjectileUpdateManager : MonoBehaviour
    {
        private static ProjectileUpdateManager instance;
        public static ProjectileUpdateManager Instance
        {
            get
            {
                //Reference equals is very performant, but won't work with destroyed objects properly: OnDestroy() contains solution
                if (ReferenceEquals(instance, null))
                {
                    instance = FindObjectOfType<ProjectileUpdateManager>();
                    if (ReferenceEquals(instance, null))
                    {
                        instance = new GameObject("Projectile Update Manager").AddComponent<ProjectileUpdateManager>();
                    }
                }
                return instance;
            }
        }
        void OnDestroy()
        {
            instance = null;
        }

        public List<IUpdateableProjectile> Projectiles { get { return projectiles; } }
        private List<IUpdateableProjectile> projectiles = new List<IUpdateableProjectile>();

        private float LastUpdateTime = 0.0f;
        //Cached variables
        protected BallisticSettings ballisticSettings;

        void Start()
        {
            LastUpdateTime = Time.time;
            ballisticSettings = BallisticSettings.Instance;
        }

        void Update()
        {
            float UpdateTimestep = 1.0f / GlobalSettings.Update.Rate;

            float StartTime = Time.realtimeSinceStartup;
            int UpdatesRequired = (int)((Time.time - LastUpdateTime) / UpdateTimestep);

            for (int n = 0; n < UpdatesRequired; n++)
            {
                UpdateAll(UpdateTimestep);

                float ExecutionTime = Time.realtimeSinceStartup - StartTime;
                float IterationTime = ExecutionTime / (n + 1);
                if (ExecutionTime + IterationTime >= GlobalSettings.Update.MaxAllowedTime) //If next update can exceed MaxAllowedTimestep: stop calculations.
                {
                    int UpdatesTotal = UpdatesRequired * projectiles.Count;
                    float Slowdown = (float)UpdatesRequired / (n + 1);
                    pDebug.LogWarning(string.Format("Too much updates require in this frame: {0}[{1}]. Calculation breaked on {2} iteration.\n Execution time: {3}ms. Slowdown: {4}x", 
                        pDebug.Int(UpdatesRequired), pDebug.Int(UpdatesTotal), pDebug.Int(n + 1), pDebug.Int(Mathf.RoundToInt(ExecutionTime * 1000)), pDebug.Float(Slowdown)), gameObject);
                    break;
                }
            }
            LastUpdateTime += UpdateTimestep * UpdatesRequired;
        }
        void UpdateAll(float deltaTime)
        {
            for (int i = 0; i < projectiles.Count; i++)
            {
                projectiles[i].UpdateProjectile(deltaTime);
            }
        }

        public void Subscribe(IUpdateableProjectile projectile)
        {
            projectiles.Add(projectile);
        }
        public void Unsubscribe(IUpdateableProjectile projectile)
        {
            projectiles.Remove(projectile);
        }

#if UNITY_EDITOR
        [MenuItem("GameObject/Projectile Ballistics/Update Manager", false, 49)]
        static void CreateUpdateManager()
        {
            GameObject obj = new GameObject("Update Manager");
            obj.AddComponent<ProjectileUpdateManager>();
            Selection.activeGameObject = obj;
        }
#endif
    }
}
