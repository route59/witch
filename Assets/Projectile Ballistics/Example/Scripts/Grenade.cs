﻿using UnityEngine;
using ProjectileBallistics.Pooling;

namespace ProjectileBallistics.Examples
{
    [AddComponentMenu("Projectile Ballistics/Examples/Grenade")]
    public class Grenade : MonoBehaviour, IPoolable
    {
        public float Timer = 5;
        public int FragmentCount = 100;
        public Vector2 VerticalRange = new Vector2(-20, 5);
        public GameObject FragmentPrefab;
        public GameObject ExplosionEffectPrefab;
        [Header("Performance")]
        public bool UseFragmentPooling = true;
        public bool UseVFXPooling = false;

        public bool Pooled { get; set; }

        void OnEnable()
        {
            Invoke("Explode", Timer);
        }

        void Explode()
        {
            if (ExplosionEffectPrefab != null)
            {
                if (UseVFXPooling || GlobalSettings.Examples.ForcePooling)
                {
                    PoolingSystem.Instance.Create(ExplosionEffectPrefab, transform.position, ExplosionEffectPrefab.transform.rotation);
                }
                else
                {
                    Instantiate(ExplosionEffectPrefab, transform.position, ExplosionEffectPrefab.transform.rotation);
                }
            }
            if (FragmentPrefab != null)
            {
                float rotX, rotY, deltaY = 360f / FragmentCount;
                Quaternion rotation;
                for (int i = 0; i < FragmentCount; i++)
                {
                    rotX = Random.Range(VerticalRange.x, VerticalRange.y);
                    rotY = (i * deltaY) + Random.Range(deltaY / -2, deltaY / 2);

                    rotation = Quaternion.Euler(new Vector3(rotX, rotY, 0));

                    if (UseFragmentPooling || GlobalSettings.Examples.ForcePooling)
                    {
                        PoolingSystem.Instance.Create(FragmentPrefab, transform.position, rotation);
                    } else
                    {
                        Instantiate(FragmentPrefab, transform.position, rotation);
                    }
                }
            }


            gameObject.SetActive(false);
            Despawn();
        }

        void Despawn()
        {
            if(!Pooled)
            {
                Destroy(gameObject);
            } else if(gameObject.activeSelf)
            {
                gameObject.SetActive(false);
            }
        }
    }
}
