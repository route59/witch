﻿using UnityEngine;
using UnityEngine.UI;

namespace ProjectileBallistics.Examples
{
    [RequireComponent(typeof(Text))]
    public class LoadInfo : MonoBehaviour
    {
        private Text text;
        private ProjectileUpdateManager updateManager;
	    
	    void Start ()
        {
            text = GetComponent<Text>();
            updateManager = ProjectileUpdateManager.Instance;
	    }
	
	    void Update ()
        {
            text.text = string.Format("{0} Projectiles({1} Updates/Second)", updateManager.Projectiles.Count, GlobalSettings.Update.Rate);
	    }
    }
}
