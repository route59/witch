﻿using UnityEngine;
using ProjectileBallistics.Pooling;

namespace ProjectileBallistics.Examples
{
    [AddComponentMenu("Projectile Ballistics/Examples/Destroy by timer")]
    public class DestroyTimer : MonoBehaviour, IPoolable
    {
        public float Lifetime = 10.0f;
        public bool Pooled { get; set; }

        void OnEnable()
        {
            Invoke("Despawn", Lifetime);
        }

        public void Initialize(float lifetime, bool pooled)
        {
            CancelInvoke("Despawn");

            Lifetime = lifetime;
            Pooled = pooled;

            Invoke("Despawn", Lifetime);
        }

        void Despawn()
        {
            if (!Pooled)
            {
                Destroy(gameObject);
            }
            else if (gameObject.activeSelf)
            {
                gameObject.SetActive(false);
            }
        }
    }
}
