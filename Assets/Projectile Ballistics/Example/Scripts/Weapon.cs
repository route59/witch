﻿using UnityEngine;
using UnityEngine.UI;

namespace ProjectileBallistics.Examples
{
    [AddComponentMenu("Projectile Ballistics/Examples/Weapon")]
    public class Weapon : MonoBehaviour
    {
        [System.Serializable]
        public class WeaponType
        {
            public string Name = "No name";
            public float ShootsPerMinute = 500f;
            [Range(0, 90)]
            public float Scatter = 1f;
            public GameObject ProjectilePrefab;
        }

        public Transform ProjectileSpawnpoint;
        public Text WeaponNameText;
        public WeaponType Primary1;
        public WeaponType Primary2;
        public WeaponType Secondary;
        public WeaponType Launcher;
        public WeaponType Grenade;
        public float ThrowForce = 15;

        [Header("Performance")]
        public bool UsePooling = true;

        private WeaponType CurrentWeapon;

        private float LastShootTime = 0;

        void Start()
        {
            CurrentWeapon = Primary1;
            UpdateWeaponName();
        }

        void Update()
        {
            WeaponSwtichingLogic();

            float ShootDelay = 60.0f / CurrentWeapon.ShootsPerMinute;
            if (Input.GetMouseButton(0) && Time.time - LastShootTime >= ShootDelay)
            {
                LastShootTime = Time.time;
                Shoot();
            }
        }

        void WeaponSwtichingLogic()
        {
            if (Input.GetKey(KeyCode.Alpha1))
            {
                CurrentWeapon = Primary1;
                UpdateWeaponName();
            }
            else if (Input.GetKey(KeyCode.Alpha2))
            {
                CurrentWeapon = Primary2;
                UpdateWeaponName();
            }
            else if (Input.GetKey(KeyCode.Alpha3))
            {
                CurrentWeapon = Secondary;
                UpdateWeaponName();
            }
            else if (Input.GetKey(KeyCode.Alpha4))
            {
                CurrentWeapon = Launcher;
                UpdateWeaponName();
            }
            else if (Input.GetKey(KeyCode.Alpha5))
            {
                CurrentWeapon = Grenade;
                UpdateWeaponName();
            }
        }
        void UpdateWeaponName()
        {
            WeaponNameText.text = CurrentWeapon.Name;
        }

        void Shoot()
        {
            if (CurrentWeapon.ProjectilePrefab != null && ProjectileSpawnpoint != null)
            {
                Vector3 scatter = Random.insideUnitSphere * CurrentWeapon.Scatter * 0.5f;
                scatter.z = 0;

                GameObject projectile = null;
                if (UsePooling || GlobalSettings.Examples.ForcePooling)
                {
                    projectile = Pooling.PoolingSystem.Instance.Create(CurrentWeapon.ProjectilePrefab, transform.position, Quaternion.Euler(transform.eulerAngles + scatter)).gameObject;
                }
                else
                {
                    projectile = Instantiate(CurrentWeapon.ProjectilePrefab, ProjectileSpawnpoint.position, Quaternion.Euler(ProjectileSpawnpoint.eulerAngles + scatter));
                }

                if (CurrentWeapon == Grenade && projectile.GetComponent<Rigidbody>() != null)
                {
                    projectile.GetComponent<Rigidbody>().AddForce(transform.forward.normalized * ThrowForce, ForceMode.Impulse);
                }
            }
        }
    }
}
