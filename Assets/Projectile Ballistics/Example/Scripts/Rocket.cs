﻿using System.Collections;
using UnityEngine;

namespace ProjectileBallistics.Examples
{
    [AddComponentMenu("Projectile Ballistics/Examples/Rocket")]
    public class Rocket : Projectile
    {
        [Header("Rocket Settings")]
        public GameObject ExplosionPrefab;
        public float DestroyTimer = 5.0f;
        [Header("Acceleration")]
        public float RocketForce = 100f;
        public float AccelerationTime = 5f;

        private bool HaveFuel
        {
            get
            {
                return (Time.time - InitTime) <= AccelerationTime;
            }
        }
        private float InitTime;

        protected override void OnProjectileInitialized()
        {
            InitTime = Time.time;
            StartCoroutine(Explode(Vector3.up, DestroyTimer));
        }

        protected override void OnProjectileHit(HitInfo info)
        {
            if(info.hitType == HitType.Penetration)
            {
                StartCoroutine(Explode(Vector3.up, 0.2f));
            } else
            {
                Explode(info.EntryHit.normal);
            }
        }

        protected override Vector3 GetExternalForce()
        {
            return HaveFuel ? (transform.forward * RocketForce) : Vector3.zero;
        }

        IEnumerator Explode(Vector3 Normal, float Time)
        {
            yield return new WaitForSeconds(Time);
            Explode(Normal);
        }
        void Explode(Vector3 Normal)
        {
            if (ExplosionPrefab != null)
            {
                Instantiate(ExplosionPrefab, transform.position, Quaternion.LookRotation(Normal));
            }
            DestroyProjectile();
        }
    }
}