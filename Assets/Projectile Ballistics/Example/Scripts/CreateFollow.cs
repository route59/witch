﻿using ProjectileBallistics.Pooling;
using UnityEngine;

namespace ProjectileBallistics.Examples
{
    [AddComponentMenu("Projectile Ballistics/Examples/Create and Follow")]
    public class CreateFollow : MonoBehaviour
    {
        public GameObject Prefab;
        public float Lifetime = 20.0f;
        [Header("Performance")]
        public bool UsePooling = true;

        private GameObject Instance;
        private Transform InstanceTransform;

        void OnEnable()
        {
            if (UsePooling || GlobalSettings.Examples.ForcePooling)
            {
                Instance = PoolingSystem.Instance.Create(Prefab, transform.position, transform.rotation);

                DestroyTimer destroyTimer = Instance.GetComponent<DestroyTimer>();
                if (destroyTimer == null)
                {
                    destroyTimer = Instance.AddComponent<DestroyTimer>();
                }

                destroyTimer.Initialize(Lifetime, true);
            }
            else
            {
                Instance = Instantiate(Prefab, transform.position, transform.rotation);
                Destroy(Instance, Lifetime);
            }

            InstanceTransform = Instance.transform;
        }

        void Update()
        {
            if (Instance != null)
            {
                InstanceTransform.position = transform.position;
            }
        }
    }
}
