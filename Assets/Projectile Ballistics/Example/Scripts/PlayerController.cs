﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace ProjectileBallistics.Examples
{
    [AddComponentMenu("Projectile Ballistics/Examples/Player Controller")]
    [RequireComponent(typeof(Rigidbody))]
    public class PlayerController : MonoBehaviour
    {
        public Camera PlayerCamera;
        public GameObject CombatSimulation;
        [Header("Settings")]
        public float Speed = 2.0f;
        public float SprintSpeed = 5.0f;
        public float VerticalRotationRange = 70.0f;
        public float MouseSensivity = 2.0f;
        public float MinFOV = 5f;
        public float MaxFOV = 60f;
        [Header("UI")]
        public Text TimeDecelerationText;

        private Vector2 rotation = Vector2.zero;
        private float Zoom = 0;
        private float TimeScale = 1.0f;
        private Rigidbody body;

        void Start()
        {
            body = GetComponent<Rigidbody>();
            Cursor.lockState = CursorLockMode.Locked;
        }

        void Update()
        {
            Rotate(Time.unscaledDeltaTime);
            ControllTime(Time.deltaTime);
            ControllZoom(Time.unscaledDeltaTime);
            ControllScene();
            ControllCombatSimulation();
        }

        void FixedUpdate()
        {
            Move(Time.fixedUnscaledDeltaTime);
        }

        void Move(float deltaTime)
        {
            Vector2 input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")).normalized;
            bool isSprint = Input.GetKey(KeyCode.LeftShift);
            float speed = isSprint ? SprintSpeed : Speed;

            Vector3 offset = transform.TransformDirection(new Vector3(input.x, 0, input.y) * speed) * deltaTime;
            body.MovePosition(transform.position + offset);
        }
        void Rotate(float deltaTime)
        {
            rotation += new Vector2(Input.GetAxisRaw("Mouse X"), -Input.GetAxisRaw("Mouse Y")) * GetSensivity();
            rotation = new Vector2(rotation.x, Mathf.Clamp(rotation.y, VerticalRotationRange / -2f, VerticalRotationRange / 2f));

            transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, rotation.x, transform.localEulerAngles.z);
            PlayerCamera.transform.localEulerAngles = new Vector3(rotation.y, PlayerCamera.transform.localEulerAngles.y, PlayerCamera.transform.localEulerAngles.z);
        }
        void ControllTime(float deltaTime)
        {
            bool Delta = false;

            if (Input.GetKeyUp(KeyCode.E))
            {
                TimeScale *= 10;
                Delta = true;
            }
            if (Input.GetKeyUp(KeyCode.Q))
            {
                TimeScale /= 10;
                Delta = true;
            }

            TimeScale = Mathf.Clamp(TimeScale, 0.01f, 1.0f);
            Time.timeScale = TimeScale;
            Time.fixedDeltaTime = TimeScale / 60.0f;

            if (Delta)
            {
                TimeDecelerationText.text = string.Format("{0}x", (1f / TimeScale).ToString("0"));
            }
        }
        void ControllZoom(float deltaTime)
        {
            Zoom = Mathf.MoveTowards(Zoom, Input.GetMouseButton(1) ? 1 : 0, 5 * deltaTime);
            PlayerCamera.fieldOfView = Mathf.Lerp(MaxFOV, MinFOV, Zoom);
        }

        void ControllScene()
        {
            if (Input.GetKeyUp(KeyCode.R))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        }
        void ControllCombatSimulation()
        {
            if(Input.GetKeyUp(KeyCode.C))
            {
                CombatSimulation.SetActive(!CombatSimulation.activeSelf);
            }
        }

        float GetSensivity()
        {
            return MouseSensivity * Mathf.Lerp(1, MinFOV / MaxFOV, Zoom);
        }
    }
}
