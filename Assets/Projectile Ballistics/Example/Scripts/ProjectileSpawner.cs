﻿using UnityEngine;
using ProjectileBallistics.Pooling;

namespace ProjectileBallistics.Examples
{
    [AddComponentMenu("Projectile Ballistics/Examples/Projectile Spawner")]
    public class ProjectileSpawner : MonoBehaviour
    {
        public GameObject ProjectilePrefab;
        public AnimationCurve SpawnTime;

        [Range(0, 360)]
        public float Scatter = 20;

        [Header("Performance")]
        public bool UsePooling = true;

        private const float SafeSpawnTime = 0.05f;

        void OnEnable()
        {
            Spawn();
        }

        void Spawn()
        {
            Vector3 scatter = Random.insideUnitSphere * Scatter * 0.5f;
            scatter.z = 0;

            if (UsePooling || GlobalSettings.Examples.ForcePooling)
            {
                PoolingSystem.Instance.Create(ProjectilePrefab, transform.position, Quaternion.Euler(transform.eulerAngles + scatter));
            }
            else
            {
                Instantiate(ProjectilePrefab, transform.position, Quaternion.Euler(transform.eulerAngles + scatter));
            }

            if (gameObject.activeInHierarchy)
            {
                float NextSpawnTime = SpawnTime.Evaluate(Random.Range(0f, 1f));
                Invoke("Spawn", Mathf.Max(NextSpawnTime, SafeSpawnTime));
            }
        }
    }
}
