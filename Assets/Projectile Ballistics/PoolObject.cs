﻿using UnityEngine;

namespace ProjectileBallistics.Pooling
{
    /// <summary>
    /// Allows object to know is it pooled or not
    /// </summary>
    public interface IPoolable
    {
        bool Pooled { get; set; }
    }

    public class PoolObject : MonoBehaviour
    {
        public event DespawnEvent OnDespawn = null;

        protected PrefabEntry prefabEntry = null;

        public void Initialize(PrefabEntry _prefabEntry)
        {
            prefabEntry = _prefabEntry;

            IPoolable[] poolables = GetComponentsInChildren<IPoolable>();
            int poolablesCount = poolables.Length;
            for (int i = 0; i < poolablesCount; i++)
            {
                poolables[i].Pooled = true;
            }
        }

        protected void OnDisable()
        {
            if (OnDespawn != null)
            {
                OnDespawn.Invoke(prefabEntry, this);
            }
        }
    }
}