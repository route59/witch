﻿using System.Collections.Generic;
using UnityEngine;

/*
 * Ballistic Settings of certain material (Wood, Metal, Glass etc.)
 * Usage: Project Window -> Create -> Projectile Ballistics -> Ballistic Material
*/
namespace ProjectileBallistics
{
    [CreateAssetMenu(fileName = "New Ballistic Material", menuName = "Projectile Ballistics/Ballistic Material")]
    public class BallisticMaterialConfig : ScriptableObject
    {
        public abstract class HitEffects
        {
            [Header("Decals")]
            [Tooltip("Entry Decal prefab")]
            public GameObject EntryDecal;
            [Tooltip("Exit Decal prefab")]
            public GameObject ExitDecal;
            [Tooltip("Does projectile radius should affect decal scale? (Radius is defined in the Projectile Config)")]
            public bool ApplyProjectileSize = true;
            [Tooltip("How many times decal is larger than prefab size or projectile radius")]
            public float DecalScale = 1.0f;
            [Header("Particle Systems")]
            [Tooltip("Entry Particle System prefab")]
            public GameObject EntryParticleSystem;
            [Tooltip("Exit Particle System prefab")]
            public GameObject ExitParticleSystem;
            [Header("Sound")]
            [Tooltip("Hit Sound prefab")]
            public AudioSource HitSound;
            [Tooltip("Penetration Sound prefab")]
            public AudioSource PenetrationSound;
            [Tooltip("Ricochet Sound prefab")]
            public AudioSource RicochetSound;
        }
        [System.Serializable]
        public class EffectsDefault : HitEffects { }
        [System.Serializable]
        public class EffectsOverride : HitEffects
        {
            [Space(10), Tooltip("Target projectile")]
            public ProjectileConfig Type;
        }

        [Header("Parameters")]
        [Tooltip("Density of the material [kg/m³]")]
        public float Density = 1000;
        [Tooltip("Multiplier applied to the ricochet chance. (Chance is defined in the Projectile Config)")]
        public float RicochetMultiplier = 1.0f;
        [Header("Visuals & Sound")]
        public EffectsDefault effectsDefault;
        [Tooltip("Overrides Default Effects for the specific projectile config")]
        public List<EffectsOverride> effectsOverride = new List<EffectsOverride>();
        [Header("Pooling")]
        public bool UseDecalPooling = true;
        public bool UseVFXPooling = true;
        public bool UseSFXPooling = true;

        public GameObject GetDecal(HitPointType type, HitEffects config)
        {
            if(type == HitPointType.Entry)
            {
                return config.EntryDecal;
            } else if (type == HitPointType.Exit)
            {
                return config.ExitDecal;
            }
            return null;
        }
        public GameObject GetParticleSystem(HitPointType type, HitEffects config)
        {
            if (type == HitPointType.Entry)
            {
                return config.EntryParticleSystem;
            }
            else if (type == HitPointType.Exit)
            {
                return config.ExitParticleSystem;
            }
            return null;
        }
        public AudioSource GetAudioEffect(HitType type, HitEffects config)
        {
            if (type == HitType.Hit)
            {
                return config.HitSound;
            }
            else if (type == HitType.Penetration)
            {
                return config.PenetrationSound;
            }
            else if (type == HitType.Ricochet)
            {
                return config.RicochetSound;
            }
            return null;
        }
    }
}
