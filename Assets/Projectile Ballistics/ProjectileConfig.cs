﻿using UnityEngine;

/*
 * Projectile configuration is used to define projectile parameters within projectile component
 * Usage: Project Window -> Create -> Projectile Ballistics -> Projectile Configuration
*/
namespace ProjectileBallistics
{
    [CreateAssetMenu(fileName = "New Projectile Configuration", menuName = "Projectile Ballistics/Projectile Configuration")]
    public class ProjectileConfig : ScriptableObject
    {
        [Header("Physical Parameters")]
        [Tooltip("Start speed of the projectile [m/s]")]
        public float StartSpeed = 900.0f;
        [Tooltip("Mass of the projectile [Kg]")]
        public float Mass = 0.00356f;
        [Tooltip("Radius of the projectile [Meters]")]
        public float Radius = 0.00285f;
        [Tooltip("Length of the projectile [Meters]")]
        public float Length = 0.0127f;
        [Tooltip("Forward drag coefficient of the projectile")]
        public float ForwardDrag = 0.2f;
        [Tooltip("Sideways drag coefficient of the projectile")]
        public float SideDrag = 0.9f;

        [Header("Obstacle Behaviour")]
        [Tooltip("Max deviation of the projectile after penetration with 100% loss of energy [Angle]")]
        public float MaxDeviation = 30.0f;
        [Tooltip("Penetration ability of the projectile on start speed[Density * Meter]")]
        public float MaxPenetration = 100.0f;
        [Tooltip("Ricochet chance by the hit angle [X: 0 -> 1(90 Degrees); Y: 0 -> 1(100%) -> Infinity]")]
        public AnimationCurve RicochetChance = AnimationCurve.EaseInOut(0, 0.05f, 1, 2.0f);
        [Header("Other")]
        [Tooltip("Lifetime of the projectile [Seconds]")]
        public float Lifetime = 5.0f;
        [Tooltip("Velocity at which projectile should be killed [m/s]")]
        public float MinimalVelocity = 1.0f;
    }
}