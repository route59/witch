﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

/*
 * Allows to configure scene specific parameters
 * Usage: Attach to any object in your scene [Game Manager or any other dummy]
*/
namespace ProjectileBallistics
{
    [AddComponentMenu("Projectile Ballistics/Ballistic Settings")]
    public class BallisticSettings : MonoBehaviour
    {
        [Header("Environment")]
        [Tooltip("Velocity of the wind [m/s]")]
        public Vector3 Wind;
        [Tooltip("Density of the air [kg/m³]")]
        public float AirDensity = 1.2f;
        [Header("Projectile")]
        [Tooltip("Defines collision layers, that are able to collide with projectiles")]
        public LayerMask ProjectileCollisionLayerMask = Physics.DefaultRaycastLayers;
        [Header("Debug")]
        [Tooltip("Debug projectile's path in the Scene view? May affect performance")]
        public bool DebugPath = false;
        [Tooltip("Debug projectile's path positions in the Scene view? May affect performance")]
        public bool DebugPathPositions = false;
        [Tooltip("Lifetime of the path [Seconds]")]
        public float PathLifetime = 5.0f;
        [Tooltip("Gradient of the path [Left to Right, Max Energy to No Energy]")]
        public Gradient PathGradient;

        public void Reset()
        {
            PathGradient = new Gradient
            {
                colorKeys = new GradientColorKey[4]
                {
                    new GradientColorKey(Color.red, 0.0f),
                    new GradientColorKey(Color.yellow, 0.33f),
                    new GradientColorKey(Color.blue, 0.66f),
                    new GradientColorKey(Color.white, 1.0f)
                },
                alphaKeys = new GradientAlphaKey[3]
                {
                    new GradientAlphaKey(1.00f, 0.0f),
                    new GradientAlphaKey(1.00f, 0.75f),
                    new GradientAlphaKey(0.25f, 1f)
                },
                mode = GradientMode.Blend
            };
        }

        private static BallisticSettings instance;
        public static BallisticSettings Instance
        {
            get
            {
                //Reference equals is very performant, but won't work with destroyed objects properly: OnDestroy() contains solution
                if (ReferenceEquals(instance, null))
                {
                    instance = FindObjectOfType<BallisticSettings>();
                    if (ReferenceEquals(instance, null))
                    {
                        instance = new GameObject("Ballistic Settings").AddComponent<BallisticSettings>();
                    }
                }
                return instance;
            }
        }
        void OnDestroy()
        {
            instance = null;
        }

#if UNITY_EDITOR
        [MenuItem("GameObject/Projectile Ballistics/Ballistics Settings", false, 49)]
        static void CreateBallisticSettings()
        {
            GameObject obj = new GameObject("Ballistics Settings");
            obj.AddComponent<BallisticSettings>();
            Selection.activeGameObject = obj;
        }
#endif
    }
}
