using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public int health;
    public int maxHealth;

    public float bulletStrength;

    Color glowColor;
    MeshRenderer renderer;
    CharacterController characterController;

    // Start is called before the first frame update
    void Start()
    {
        renderer = GetComponent<MeshRenderer>();
        characterController = GetComponent<CharacterController>();
        health = maxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        glowColor = Color.Lerp(glowColor, Color.black, 0.05f);
        renderer.material.SetColor("_EmissionColor", glowColor);

        if (health <= 0)
        {
            KillEnemy();
        }
    }

    public void HitByBullet(Vector3 forceDirection, int damage)
    {
        glowColor = Color.white;
        health -= damage;
        if (health <= 0)
        {
            KillEnemy();
        }
        characterController.SimpleMove(bulletStrength * forceDirection);
    }

    public void KillEnemy()
    {
        Destroy(gameObject);
    }
}
