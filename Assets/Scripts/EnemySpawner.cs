using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public int enemyCount;
    public const int EnemyMaxCount = 20;
    public List<Transform> spawnPoints;

    public GameObject enemyPrefab;
    public Transform enemyHolder;

    public Transform player;

    private void Start()
    {
        spawnPoints.Clear();
        foreach (Transform child in transform)
        {
            spawnPoints.Add(child);
        }
    }

    private void Update()
    {
        enemyCount = enemyHolder.childCount;

        if (enemyCount < EnemyMaxCount)
        {
            int randomI = Random.Range(0, spawnPoints.Count);
            SpawnEnemy(randomI);
        }
    }

    void SpawnEnemy(int spawnPoint)
    {
        var newEnemy = Instantiate(enemyPrefab, enemyHolder);
        newEnemy.transform.position = spawnPoints[spawnPoint].position;
        //newEnemy.GetComponent<Pathfinding.AIDestinationSetter>().target = player;
        newEnemy.GetComponent<AstarAI>().targetPosition = player;
    }
}
