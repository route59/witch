using ProjectileBallistics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerProjectile : ProjectileBallistics.Projectile
{
    public int damage;
    
    protected override void OnProjectileHit(HitInfo hitInfo)
    {
        if (hitInfo.HitCollider.layer == 26)
        {
            var enemy = hitInfo.HitCollider.GetComponent<Enemy>();

            enemy.HitByBullet(Velocity, damage);
        }
    }
}
