using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class GameMasterController : MonoBehaviour
{
    public GameParameters _gameParameters;

    public static GameParameters gameParameters;


    public void Awake()
    {
        gameParameters = _gameParameters;
    }

    private void Update()
    {
        if(gameParameters == null)
        {
            gameParameters = _gameParameters;
        }


    }
}
