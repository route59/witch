using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SlowmoController : MonoBehaviour
{
    public static float slowmoTransition;
    public static float TimeScale;
    public static bool inSlowmo; // 0 = slowmo, 1 = normal
    float timeScaleOverride; // 0 or 1
    float overrideTween; // 0 ~ 1

    bool overrideSlowmo;

    public GameObject playerTopDown;
    public GameObject cameraTopDown;
    public GameObject playerThirdPerson;
    public GameObject cameraThirdPerson;

    // Start is called before the first frame update
    void Start()
    {
        slowmoTransition = 1f;
        EnterNormal();
    }

    // Update is called once per frame
    void Update()
    {
        TimeScale = Mathf.Lerp(GameMasterController.gameParameters.slowmoScale, 1, slowmoTransition);
        TimeScale = Mathf.Lerp(TimeScale, timeScaleOverride, overrideTween);
        //Physics.gravity = Vector3.down * 9.81f * TimeScale;

        if (!overrideSlowmo)
        {
            if (!inSlowmo)
            {
                if (Input.GetMouseButton(1))
                {
                    EnterSlowmo();
                }
            }
            if (inSlowmo)
            {
                if (!Input.GetMouseButton(1))
                {
                    EnterNormal();
                }
            }
        }
        //Time.fixedDeltaTime = Time.timeScale * 0.02f;
    }

    public void TempNormalSpeed()
    {
        DOTween.Kill("TempSlowmo");
        timeScaleOverride = 1;
        overrideTween = 1;
        DOTween.To(() => overrideTween, x => overrideTween = x, 0, 2f).SetUpdate(true).SetEase(Ease.OutExpo).SetId("TempSlowmo");
    }

    public void ForceSlowmo()
    {
        overrideSlowmo = true;
        EnterSlowmo();
    }

    public void ForceNormal()
    {
        overrideSlowmo = true;
        EnterNormal();
    }

    public void EndForce()
    {
        overrideSlowmo = false;
    }

    void EnterNormal()
    {
        inSlowmo = false;

        DOTween.Kill("Slowmo");
        DOTween.To(() => slowmoTransition, x => slowmoTransition = x, 1, GameMasterController.gameParameters.slowmoTransitionOut).SetUpdate(true).SetEase(Ease.InOutExpo).SetId("Slowmo");

        // turn off 3P
        playerThirdPerson.SetActive(false);
        cameraThirdPerson.SetActive(false);

        // set TD to 3P
        playerTopDown.transform.position = playerThirdPerson.transform.position;
        playerTopDown.transform.rotation = playerThirdPerson.transform.rotation;

        // turn on TD
        playerTopDown.SetActive(true);
        cameraTopDown.SetActive(true);

        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

        //DOTween.To(() => Time.timeScale, x => Time.timeScale = x, 1, GameMasterController.gameParameters.slowmoTransitionOut).SetUpdate(true);
    }

    void EnterSlowmo()
    {
        inSlowmo = true;

        DOTween.Kill("Slowmo");
        DOTween.To(() => slowmoTransition, x => slowmoTransition = x, 0, GameMasterController.gameParameters.slowmoTransitionIn).SetUpdate(true).SetEase(Ease.InOutExpo).SetId("Slowmo");

        // turn off 3P
        playerTopDown.SetActive(false);
        cameraTopDown.SetActive(false);

        // set TD to 3P
        playerThirdPerson.transform.position = playerTopDown.transform.position;
        playerThirdPerson.transform.rotation = playerTopDown.transform.rotation;

        // turn on TD
        playerThirdPerson.SetActive(true);
        cameraThirdPerson.SetActive(true);

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        //DOTween.To(() => Time.timeScale, x => Time.timeScale = x, GameMasterController.gameParameters.slowmoScale, GameMasterController.gameParameters.slowmoTransitionIn).SetUpdate(true);
    }
}
