using UnityEngine;

public static class AdjustedLerp
{
    public static float FloatAdjustedLerp(this float baseLerp, bool affectedByTimeScale = false)
    {
        float effectiveFrames = Time.deltaTime * (affectedByTimeScale ? 1 : Time.timeScale) / (1 / 60.0f);
        return 1 - Mathf.Pow((1.0f - baseLerp), effectiveFrames);
    }
}
