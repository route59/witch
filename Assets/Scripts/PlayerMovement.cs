using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PlayerMovement : MonoBehaviour
{
    public GameMasterController masterController;
    GameParameters gameParameters;
    public Rigidbody rigidbody;
    public Collider collider;

    public Transform cameraRig;

    public PlayerState playerState;

    Vector3 hookTarget;

    public float hookSpeed;
    float hookTimer;

    public GameObject hooklineObject;

    public Vector2 moveAxis;
    public Vector3 mouseTarget;

    public bool IsGrounded()
    {
        return Physics.Raycast(transform.position, -Vector3.up, collider.bounds.extents.y + 1f);
    }

    public enum PlayerState
    {
        move,
        boost,
        hookshot
    }

    // Start is called before the first frame update
    void Start()
    {
        masterController = FindObjectOfType<GameMasterController>();
        gameParameters = GameMasterController.gameParameters;

        playerState = PlayerState.move;
    }

    // Update is called once per frame
    void Update()
    {
        moveAxis = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

        Plane plane = new Plane(Vector3.up, transform.position);
        Ray mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        float distance;
        if (plane.Raycast(mouseRay, out distance))
        {
            mouseTarget = mouseRay.GetPoint(distance);
            mouseTarget = new Vector3(mouseTarget.x, transform.position.y, mouseTarget.z);
        }

        if (playerState == PlayerState.move)
        {
            if (IsGrounded())
            {
                Move(moveAxis);
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    StopCoroutine("Boost");
                    StartCoroutine(Boost(moveAxis));
                }
            }
        }
        if (playerState == PlayerState.hookshot)
        {
            hookTimer -= Time.deltaTime;

            // find difference vector
            Vector3 diffVector = hookTarget - transform.position;

            if (hookTimer <= 0 || diffVector.magnitude <= 1f)
            {
                // end hookshot
                EndHookshot();
            }
            else
            {
                rigidbody.velocity = diffVector.normalized * gameParameters.hookSpeed;
            }

            Vector3 originalHookTarget = hookTarget - Vector3.up * gameParameters.hookOffset.y;

            hooklineObject.SetActive(true);
            hooklineObject.transform.position = (transform.position + originalHookTarget) / 2f;
            hooklineObject.transform.LookAt(transform.position);
            hooklineObject.transform.localScale = new Vector3(0.1f, 0.1f, (transform.position - originalHookTarget).magnitude / 2);
        }
        else
        {
            hooklineObject.SetActive(false);
        }

        Aim();
    }

    // Rotate based on mouse position
    void Aim()
    {
        transform.LookAt(mouseTarget, Vector3.up);
    }

    public void Hookshot(Vector3 target)
    {
        hookTarget = target;
        playerState = PlayerState.hookshot;

        hookTimer = gameParameters.hookDuration;
    }

    void EndHookshot()
    {
        playerState = PlayerState.move;
        rigidbody.velocity = Vector3.right * 0.3f * rigidbody.velocity.x + Vector3.up * -9f + Vector3.forward * rigidbody.velocity.z * 0.3f;

        FindObjectOfType<SlowmoController>().EndForce();
    }

    void Move(Vector2 moveAxis)
    {
        Vector3 moveVector = cameraRig.right * moveAxis.x + cameraRig.forward * moveAxis.y;
        
        rigidbody.velocity = moveVector * gameParameters.playerSpeed + rigidbody.velocity.y * Vector3.up;
    }

    IEnumerator Boost(Vector2 moveAxis)
    {
        playerState = PlayerState.boost;
        Vector3 moveVector = cameraRig.right * moveAxis.x + cameraRig.forward * moveAxis.y;
        rigidbody.velocity = moveVector * gameParameters.boostSpeed;

        yield return new WaitForSeconds(gameParameters.boostDuration);

        playerState = PlayerState.move;
        rigidbody.velocity = Vector3.zero;
    }
}
