using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ProjectileBallistics;
using Thinksquirrel.CShake;

public class PlayerShoot : MonoBehaviour
{
    public GameObject projectilePrefab;
    public Transform bulletSpawnPoint;

    public CameraShake shaker;
    public CameraShake UIshaker;

    private float LastShootTime = 0;

    // Update is called once per frame
    void Update()
    {
        float ShootDelay = 60.0f / GameMasterController.gameParameters.fireRate;
        if (Input.GetMouseButton(0) && Time.time - LastShootTime >= ShootDelay)
        {
            LastShootTime = Time.time;
            Shoot();
        }
    }


    void Shoot()
    {
        GameObject projectile = Instantiate(projectilePrefab, bulletSpawnPoint.position, Quaternion.Euler(bulletSpawnPoint.eulerAngles));
        shaker.Shake();
        UIshaker.Shake();
    }
}
