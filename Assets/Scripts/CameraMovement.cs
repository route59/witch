using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CameraMovement : MonoBehaviour
{
    [Header("Move Camera")]
    public Transform moveRig; // based on player position
    public Transform slideRig; // based on player velocity
    public Transform panRig; // based on mouse position
    public Camera moveCam;

    [Header("Zoom Camera")]
    public Camera zoomCam;

    [Header("Real Camera")]
    public Transform realRig;
    public Camera realCam;

    [Header("Players")]
    public GameObject playerTopDown;
    public GameObject playerThirdPerson;

    GameParameters gameParameters;
    public PlayerMovement playerMovement;


    Vector3 setPosition;
    Quaternion setRotation;
    float fov;

    private void Start()
    {
        gameParameters = GameMasterController.gameParameters;

        playerMovement = FindObjectOfType<PlayerMovement>();
    }

    private void Update()
    {
        // Move Camera
        Vector3 toMove = playerMovement.transform.position;
        moveRig.transform.position = Vector3.Lerp(moveRig.transform.position, toMove, gameParameters.moveSpeed.FloatAdjustedLerp());

        Vector3 toSlider = new Vector3(playerMovement.moveAxis.x, 0, playerMovement.moveAxis.y);// playerMovement.rigidbody.velocity;
        slideRig.transform.localPosition = Vector3.Lerp(slideRig.transform.localPosition, toSlider * gameParameters.slideStrength, gameParameters.slideSpeed.FloatAdjustedLerp());

        Vector3 toPan = gameParameters.panStrength * new Vector3(Mathf.Clamp((Input.mousePosition.x / Screen.width), 0, 1) - 0.5f, 0f, Mathf.Clamp((Input.mousePosition.y / Screen.height), 0, 1) - 0.5f);
        panRig.transform.localPosition = Vector3.Lerp(panRig.transform.localPosition, toPan, gameParameters.panSpeed.FloatAdjustedLerp());

        if (SlowmoController.slowmoTransition >= 0.99f)
        {
            realRig.position = moveCam.transform.position;
            realRig.rotation = moveCam.transform.rotation;
            realCam.fieldOfView = moveCam.fieldOfView;
        }
        else if (SlowmoController.slowmoTransition <= 0.01f)
        {
            realRig.position = zoomCam.transform.position;
            realRig.rotation = zoomCam.transform.rotation;
            realCam.fieldOfView = zoomCam.fieldOfView;
        }
        else
        {
            realRig.position = Vector3.Lerp(zoomCam.transform.position, moveCam.transform.position, SlowmoController.slowmoTransition);
            realRig.rotation = Quaternion.Lerp(zoomCam.transform.rotation, moveCam.transform.rotation, SlowmoController.slowmoTransition);
            realCam.fieldOfView = Mathf.Lerp(zoomCam.fieldOfView, moveCam.fieldOfView, SlowmoController.slowmoTransition);
        }

        //moveRig.transform.rotation = Quaternion.Lerp(playerTopDown.transform.rotation, Quaternion.identity, SlowmoController.slowmoTransition);

        moveRig.transform.rotation = Quaternion.Lerp(playerTopDown.transform.rotation, Quaternion.Lerp(moveRig.transform.rotation, playerTopDown.transform.rotation, 0.001f), SlowmoController.slowmoTransition);
    }
}