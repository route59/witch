using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "GameParameters")]
public class GameParameters : ScriptableObject
{
    [Header("Slowmo")]
    public float slowmoScale;
    public float slowmoTransitionIn;
    public float slowmoTransitionOut;

    [Header("Player Movement")]
    public float playerSpeed;
    public float boostSpeed;
    public float boostDuration;
    public float hookSpeed;
    public Vector3 hookOffset;
    public float hookDuration;

    [Header("Bullet")]
    public float fireRate;

    [Header("Move Camera")]
    public float moveSpeed;
    public float slideStrength;
    public float slideSpeed;
    public float panStrength;
    public float panSpeed;
}
