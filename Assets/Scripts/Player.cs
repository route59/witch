using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public GameObject player3p;
    public GameObject playertd;
    public Transform dummy;

    // Update is called once per frame
    void Update()
    {
        if (SlowmoController.inSlowmo)
        {
            dummy.position = player3p.transform.position;
        }
        else
        {
            dummy.position = playertd.transform.position;
        }
    }
}
