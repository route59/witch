using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class PlayerHookshot : MonoBehaviour
{
    public bool hasTarget;
    public Camera slowmoCam;

    public GameMasterController masterController;
    GameParameters gameParameters;

    public GameObject dummy;
    public PlayerMovement playerMovement;

    public float hookLength;
    public LayerMask hookLayerMask;

    Vector3 outPos;

    public Image reticle;

    public bool aimingHookshot;

    private void Start()
    {
        masterController = FindObjectOfType<GameMasterController>();
        gameParameters = GameMasterController.gameParameters;
    }

    void Update()
    {
        hasTarget = false;
        if (!aimingHookshot && Input.GetKey(KeyCode.Q))
        {
            AimHookshot();
        }
        if (aimingHookshot)
        {
            // casts ray forward
            RaycastHit outRay;
            if (Physics.Raycast(slowmoCam.transform.position, slowmoCam.transform.forward, out outRay, hookLength, hookLayerMask))
            {
                hasTarget = true;
                outPos = outRay.point;
                dummy.transform.position = outPos;
            }

            if (!Input.GetKey(KeyCode.Q))
            {
                ReleaseHookshot();
            }
        }

        if (hasTarget)
        {
            reticle.color = Color.Lerp(reticle.color, Color.red, 0.2f);
        }
        else
        {
            reticle.color = Color.Lerp(reticle.color, Color.white, 0.2f);
        }
    }

    // Update is called once per frame
    public void AimHookshot()
    {
        aimingHookshot = true;
    }

    public void ReleaseHookshot()
    {
        if (hasTarget)
        {
            // determine endpoint
            Vector3 relativeOffset = Vector3.up * gameParameters.hookOffset.y;

            Vector3 endPoint = outPos + relativeOffset;

            // forces player out of slowmo
            FindObjectOfType<SlowmoController>().ForceNormal();

            // shoots player forward to target
            playerMovement.Hookshot(endPoint);
        }

        EndAim();
    }

    public void EndAim()
    {
        aimingHookshot = false;
    }
}