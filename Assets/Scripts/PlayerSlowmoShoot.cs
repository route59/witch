using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Thinksquirrel.CShake;

public class PlayerSlowmoShoot : MonoBehaviour
{
    public GameObject projectilePrefab;
    public Transform bulletSpawnPoint;

    public Camera slowmoCam;

    public CameraShake shaker;
    public CameraShake UIshaker;

    public GameObject dummy;
    public float LastShootTime = 0;

    // Update is called once per frame
    void Update()
    {
        // constantly rotates to aim towards mouse cursor
        RaycastHit outRay;
        dummy.transform.position = slowmoCam.transform.position + slowmoCam.transform.forward * 100f;

        Debug.DrawLine(slowmoCam.transform.position, slowmoCam.transform.forward);

        if (Physics.Raycast(slowmoCam.transform.position, slowmoCam.transform.forward, out outRay))
        {
            dummy.transform.position = outRay.point;
        }

        transform.LookAt(dummy.transform, Vector3.up);

        LastShootTime += Time.deltaTime;
        if (Input.GetMouseButtonDown(0) && LastShootTime > 0.6f)
        {
            LastShootTime = 0;
            Shoot();
        }
    }

    void Shoot()
    {
        GameObject projectile = Instantiate(projectilePrefab, bulletSpawnPoint.position, Quaternion.Euler(bulletSpawnPoint.eulerAngles));
        shaker.Shake();
        UIshaker.Shake();

        FindObjectOfType<SlowmoController>().TempNormalSpeed();
    }
}
